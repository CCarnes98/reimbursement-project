package main.toplayer.dao;

import java.util.List;
import main.toplayer.model.*;

public interface ReimbursementDao {
	
			//inserts
			public boolean insertEmployee(Employee employee);
			public boolean insertTicket(Ticket ticket);
			
			//selects
			public Employee selectEmployee(UserAccount loginUser);
			public List<Employee> selectAllEmployee();
			public Ticket selectTicket(int ticketId);
			public List<Ticket> selectAllTickets();
			
			//updates
			public boolean UpdateTicketStatus(int ticketId, String status);
			
			//delete
			public boolean deleteEmployee(int employeeId);
			public boolean deleteTicket(int ticketId);
			

}

package main.toplayer.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import main.toplayer.model.Employee;
import main.toplayer.model.Ticket;
import main.toplayer.model.UserAccount;

public class ReimbursementDaoImpl implements ReimbursementDao{
	
	static { 
        try {
            Class.forName("org.postgresql.Driver");
        }catch(ClassNotFoundException e) {
            e.printStackTrace();
            System.out.println("Static block has failed me");
        }
    }
	
	@Override
	public boolean insertEmployee(Employee employee) {
		try(Connection conn = OurCustomConnectionFactory.getConnection()) {
			
			String ourSQLStatement = "INSERT INTO employees VALUES(DEFAULT,?,?,'employee')";
			
			PreparedStatement ps = conn.prepareStatement(ourSQLStatement);
			ps.setString(1, employee.getUsername()); 
			ps.setString(2, employee.getPassword());  
			
			ps.executeUpdate();
			
			return true;
		
		}catch(SQLException e) {
			e.printStackTrace();
			
			return false;
		}
	}

	@Override
	public boolean insertTicket(Ticket ticket) {
		try(Connection conn = OurCustomConnectionFactory.getConnection()) {
					
			String ourSQLStatement = "INSERT INTO tickets VALUES(DEFAULT,?,?,?,?)";
					
			PreparedStatement ps = conn.prepareStatement(ourSQLStatement);
			ps.setInt(1, ticket.getAmount());
			ps.setString(2, ticket.getDescription()); 
			ps.setString(3, ticket.getStatus());
			ps.setInt(4, ticket.getEmployeeFK());
					
			ps.executeUpdate();
					
			return true;
				
		}catch(SQLException e) {
			e.printStackTrace();
					
			return false;
		}
	}

	@Override
	public Employee selectEmployee(UserAccount loginUser) {
		Employee emp = null;
		try(Connection conn = OurCustomConnectionFactory.getConnection()) {
			String ourSQLStatement = "SELECT * FROM employees WHERE username='"+
					loginUser.getUsername()+"' AND pass_word='"+loginUser.getPassword()+"';";
			
			PreparedStatement ps = conn.prepareStatement(ourSQLStatement);

			ResultSet resultSet = ps.executeQuery();
			
			if(resultSet.next()) {
				emp =  new Employee(resultSet.getInt(1), 
						resultSet.getString(2), resultSet.getString(3), 
						resultSet.getString(4));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return emp;
	}

	@Override
	public List<Employee> selectAllEmployee() {
		List<Employee> empList = new ArrayList<>();
		try(Connection conn = OurCustomConnectionFactory.getConnection()) {
			String ourSQLStatement = "SELECT * FROM employees;";
			
			PreparedStatement ps = conn.prepareStatement(ourSQLStatement);
			ResultSet resultSet = ps.executeQuery();
			
			while(resultSet.next()){
				Employee newEmployee = new Employee(resultSet.getInt(1), 
						resultSet.getString(2), resultSet.getString(3), 
						resultSet.getString(4));
				empList.add(newEmployee);
			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
		
		return empList;
	}

	@Override
	public Ticket selectTicket(int ticketId) {
		Ticket tic = null;
		try(Connection conn = OurCustomConnectionFactory.getConnection()) {
			String ourSQLStatement = "SELECT employees.username,tickets.ticketid , "
					+ "tickets.amount, tickets.description, tickets.status, tickets.employeeid "
					+ "FROM employees INNER JOIN tickets ON "
					+ "tickets.employeeId=employees.employeeid "
					+ "WHERE ticketId='"+ticketId+"';";
			
			PreparedStatement ps = conn.prepareStatement(ourSQLStatement);
			
			ResultSet resultSet = ps.executeQuery();
			
			if(resultSet.next()) {
				tic = new Ticket(resultSet.getString(1),resultSet.getInt(2), 
						resultSet.getInt(3), resultSet.getString(4), 
						resultSet.getString(5), resultSet.getInt(6));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return tic;
	}

	@Override
	public List<Ticket> selectAllTickets() {
		List<Ticket> ticList = new ArrayList<>();
		try(Connection conn = OurCustomConnectionFactory.getConnection()) {
			String ourSQLStatement = "SELECT employees.username,tickets.ticketid , "
					+ "tickets.amount, tickets.description, tickets.status, tickets.employeeid  "
					+ "FROM employees INNER JOIN tickets "
					+ "ON tickets.employeeId=employees.employeeid ;";
			
			PreparedStatement ps = conn.prepareStatement(ourSQLStatement);
			
			ResultSet resultSet = ps.executeQuery();
			
			while(resultSet.next()){
				Ticket Tic = new Ticket(resultSet.getString(1),resultSet.getInt(2), 
						resultSet.getInt(3), resultSet.getString(4), 
						resultSet.getString(5), resultSet.getInt(6));
				ticList.add(Tic);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return ticList;
	}
	
	@Override
		public boolean UpdateTicketStatus(int ticketId, String status) {
			try(Connection conn = OurCustomConnectionFactory.getConnection()){
				String ourSQLStatement = "UPDATE tickets SET status='"+status+"' WHERE ticketId='"+ticketId+"';";
				
				PreparedStatement ps = conn.prepareStatement(ourSQLStatement);
				
				ps.executeUpdate();
				return true;
			} catch (SQLException e) {
				e.printStackTrace();
				return false;
			}
		}
	
	@Override
	public boolean deleteEmployee(int employeeId) {
		try(Connection conn = OurCustomConnectionFactory.getConnection()) {
			String ourSQLStatement = "DELETE FROM employees WHERE employeeId='"+employeeId+"';";
			
			PreparedStatement ps = conn.prepareStatement(ourSQLStatement);
			
			ps.executeUpdate();
			
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean deleteTicket(int ticketId) {
		try(Connection conn = OurCustomConnectionFactory.getConnection()) {
			String ourSQLStatement = "DELETE FROM tickets WHERE ticketId='"+ticketId+"';";
			
			PreparedStatement ps = conn.prepareStatement(ourSQLStatement);
			
			ps.executeUpdate();
			
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

}

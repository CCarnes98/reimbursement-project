package main.toplayer.frontcontroller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import main.toplayer.controller.JsonController;
import main.toplayer.controller.LoginController;
import main.toplayer.controller.TicketController;

public class Dispatcher {
	
	public static void myVirtualRouterMethod(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		System.out.println("\n\n\tIN OUR DISPATCHER ( myVirtualRouter() )");

		System.out.println("Current URL: "+ req.getRequestURL());
		System.out.println("Current URI: "+ req.getRequestURI());
		
		switch (req.getRequestURI()) {
			case"/chandler-carnes/json/addticket":
				System.out.println("case1");
				JsonController.addTicketToDataBase(req, resp);
				break;
			case"/chandler-carnes/json/addemployee":
				System.out.println("case2");
				JsonController.addEmployeeToDataBase(req, resp);
				break;
			case"/chandler-carnes/ticket/list":
				System.out.println("case3");
				TicketController.listAllTickets(req, resp);
				break;
			case "/chandler-carnes/ticket/status":
				System.out.println("case4");
				TicketController.updateTicketStatus(req, resp);
				break;
			case "/chandler-carnes/login/login":
				System.out.println("case5");
				LoginController.loginDirectResponseJson(req, resp);
				break;
			case "/chandler-carnes/login/logout":
				System.out.println("case6");
				LoginController.logout(req, resp);
				break;
			default:
				System.out.println("no controller for uri handle");
					
		}
	}

}

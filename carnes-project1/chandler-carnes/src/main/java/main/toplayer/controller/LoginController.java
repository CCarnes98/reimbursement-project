package main.toplayer.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.fasterxml.jackson.databind.ObjectMapper;

import main.toplayer.model.UserAccount;
import main.toplayer.model.Employee;
import main.toplayer.server.*;

public class LoginController {
	
	public static void logout(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		HttpSession session = req.getSession();
		session.invalidate();
		
		resp.setContentType("text/html");
		PrintWriter printer = resp.getWriter();
		printer.println("You've successfully logged out");
	}
	
	public static void loginDirectResponseJson(HttpServletRequest req, HttpServletResponse resp) throws IOException {

		if( ! req.getMethod().equals("POST") ) {
			resp.getWriter().println("Error input not received within post method");
			return;
		}
		
		ReimbursementService s = new ReimbursementServiceImpl();

		ObjectMapper mapper = new ObjectMapper();
		
		UserAccount formInputUserObject = mapper.readValue(req.getInputStream(), UserAccount.class);
		System.out.println("Using a json from the user");
		
		Employee actualUser = s.getEmployee(formInputUserObject);
		
		if(actualUser != null) {
			//they successfully logged in
			System.out.println(actualUser);
			req.getSession().setAttribute("currentUser", actualUser);
			resp.getWriter().println("You successfully logged in, "+ actualUser.getUsername());
		}else {
			resp.getWriter().println("invalid username or password");
		}
	}
}

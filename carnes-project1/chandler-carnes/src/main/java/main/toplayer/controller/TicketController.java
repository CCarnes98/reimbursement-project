package main.toplayer.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import main.toplayer.model.Employee;
import main.toplayer.model.Ticket;
import main.toplayer.server.ReimbursementService;
import main.toplayer.server.ReimbursementServiceImpl;

public class TicketController {
	
	
	
	public static void listAllTickets(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		Employee sessionUser = (Employee)req.getSession().getAttribute("currentUser");
		
		if( ! req.getMethod().equals("POST") ) {
			resp.getWriter().println("Error input not received within post method");
			return;
		}
		
		if (sessionUser == null || (!sessionUser.getEmployeeType().equals("manager")) ) {
		resp.getWriter().println("warning, manager access required");
			return;
		}
		
		ReimbursementService myServ = new ReimbursementServiceImpl();
		
		List<Ticket> myList = myServ.getAllTickets();
		
		resp.setContentType("application/json");
		String myJson = new ObjectMapper().writeValueAsString(myList);
		
		PrintWriter printer = resp.getWriter();
		printer.println(myJson);
	}
	
	public static void updateTicketStatus(HttpServletRequest req, HttpServletResponse resp) throws IOException{
		Employee sessionUser = (Employee)req.getSession().getAttribute("currentUser");
		
		if( ! req.getMethod().equals("POST") ) {
			resp.getWriter().println("Error input not received within post method");
			return;
		}
		
		if (sessionUser == null || (!sessionUser.getEmployeeType().equals("manager")) ) {
			resp.getWriter().println("warning, manager access required");
				return;
		}
		
		
		int ticketId = Integer.parseInt(req.getParameter("ticketId"));
		String password = req.getParameter("status");
		
		ReimbursementService myServ = new ReimbursementServiceImpl();
		
		boolean update = myServ.updateTicketStatus(ticketId, password);
		
		resp.setContentType("application/json");
		String myJson = new ObjectMapper().writeValueAsString(update);
		
		PrintWriter printer = resp.getWriter();
		printer.println(myJson);
		
		
	}

}

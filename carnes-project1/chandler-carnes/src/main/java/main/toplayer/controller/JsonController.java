package main.toplayer.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import main.toplayer.model.Employee;
import main.toplayer.model.Ticket;
import main.toplayer.server.ReimbursementService;
import main.toplayer.server.ReimbursementServiceImpl;

public class JsonController {
	
	public static void addEmployeeToDataBase(HttpServletRequest req, HttpServletResponse resp) throws IOException{
		
		Employee sessionUser = (Employee)req.getSession().getAttribute("currentUser");
		
		if( ! req.getMethod().equals("POST") ) {
			resp.getWriter().println("Error input not received within post method");
			return;
		}
		
		if (sessionUser == null || (!sessionUser.getEmployeeType().equals("manager")) ) {
		resp.getWriter().println("warning, manager access required");
			return;
		}
		
		ReimbursementService myServ = new ReimbursementServiceImpl();
		
		ObjectMapper mapper = new ObjectMapper();
		
		Employee formInputUserObject = mapper.readValue(req.getInputStream(), Employee.class);
		
		boolean update = myServ.AddEmployee(formInputUserObject);
		
		resp.setContentType("application/json");
		String myJson = new ObjectMapper().writeValueAsString(update);
		
		PrintWriter printer = resp.getWriter();
		printer.println(myJson);
		
		
	}
	
public static void addTicketToDataBase(HttpServletRequest req, HttpServletResponse resp) throws IOException{
		
		Employee sessionUser = (Employee)req.getSession().getAttribute("currentUser");
		
		if( ! req.getMethod().equals("POST") ) {
			resp.getWriter().println("Error input not received within post method");
			return;
		}
		
		if (sessionUser == null || (!sessionUser.getEmployeeType().equals("employee")) ) {
		resp.getWriter().println("warning, managers cannot insert tickets");
			return;
		}
		
		ReimbursementService myServ = new ReimbursementServiceImpl();
		
		ObjectMapper mapper = new ObjectMapper();
		
		Ticket formInputUserObject = mapper.readValue(req.getInputStream(), Ticket.class);
		formInputUserObject.setStatus("pending");
		formInputUserObject.setEmployeeFK(sessionUser.getEmployeeId());
		
		boolean update = myServ.AddTicket(formInputUserObject);
		
		resp.setContentType("application/json");
		String myJson = new ObjectMapper().writeValueAsString(update);
		
		PrintWriter printer = resp.getWriter();
		printer.println(myJson);
		
		
	}

}

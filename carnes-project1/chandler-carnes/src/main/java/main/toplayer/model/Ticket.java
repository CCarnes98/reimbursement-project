package main.toplayer.model;

public class Ticket {
	private String username;
	private int ticketId;
	private int amount;
	private String description;
	private String status;
	private int employeeFK;
	
	public Ticket() {
		
	}

	public Ticket(String username, int tickerId, int amount, String description, String status, int employeeFK) {
		super();
		this.username = username;
		this.ticketId = tickerId;
		this.amount = amount;
		this.description = description;
		this.status = status;
		this.employeeFK = employeeFK;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getTicketId() {
		return ticketId;
	}

	public void setTicketId(int tickerId) {
		this.ticketId = tickerId;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getEmployeeFK() {
		return employeeFK;
	}

	public void setEmployeeFK(int employeeFK) {
		this.employeeFK = employeeFK;
	}

	@Override
	public String toString() {
		return "Ticket [username=" + username + ", ticketId=" + ticketId + ", amount=" + amount + ", description="
				+ description + ", status=" + status + ", employeeFK=" + employeeFK + "]";
	}
	
}

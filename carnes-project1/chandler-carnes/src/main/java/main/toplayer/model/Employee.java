package main.toplayer.model;

public class Employee {
	private int EmployeeId;
	private String username;
	private String password;
	private String EmployeeType;
	
	public Employee() {
		
	}

	public Employee(int employeeId, String firstName, String lastname, String employeeType) {
		super();
		EmployeeId = employeeId;
		this.username = firstName;
		this.password = lastname;
		EmployeeType = employeeType;
	}

	public int getEmployeeId() {
		return EmployeeId;
	}

	public void setEmployeeId(int employeeId) {
		EmployeeId = employeeId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String firstName) {
		this.username = firstName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String lastname) {
		this.password = lastname;
	}

	public String getEmployeeType() {
		return EmployeeType;
	}

	public void setEmployeeType(String employeeType) {
		EmployeeType = employeeType;
	}

	@Override
	public String toString() {
		return "Employee [EmployeeId=" + EmployeeId + ", firstName=" + username + ", lastname=" + password
				+ ", EmployeeType=" + EmployeeType + "]";
	}
	
}

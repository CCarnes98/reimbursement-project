package main.toplayer.server;

import java.util.List;

import main.toplayer.dao.ReimbursementDao;
import main.toplayer.dao.ReimbursementDaoImpl;
import main.toplayer.model.Employee;
import main.toplayer.model.Ticket;
import main.toplayer.model.UserAccount;

public class ReimbursementServiceImpl implements ReimbursementService {

	ReimbursementDao reburseObj = new ReimbursementDaoImpl();
	
	@Override
	public Boolean AddEmployee(Employee employee) {

		return reburseObj.insertEmployee(employee);
	}

	@Override
	public Boolean AddTicket(Ticket ticket) {

		return reburseObj.insertTicket(ticket);
	}

	@Override
	public Employee getEmployee(UserAccount loginUser) {
		
		return reburseObj.selectEmployee(loginUser);
	}

	@Override
	public List<Employee> getAllEmployees() {

		return reburseObj.selectAllEmployee();
	}

	@Override
	public Ticket getTicket(int ticketId) {
		
		return reburseObj.selectTicket(ticketId);
	}

	@Override
	public List<Ticket> getAllTickets() {

		return reburseObj.selectAllTickets();
	}
	@Override
	public boolean updateTicketStatus(int ticketid, String status) {
		
		Ticket temp = reburseObj.selectTicket(ticketid);
		String tempStat = temp.getStatus();
		if(tempStat.equals("approved") || tempStat.equals("denied")) {
			System.out.println("ticket has already been edited");
			return false;
		}
		if(status.equals("pending")) {
			System.out.println("ticket cannot change back to pending");
			return false;
		}
		return reburseObj.UpdateTicketStatus(ticketid, status);
	}
	@Override
	public boolean removeEmployee(int employeeId) {
		
		return reburseObj.deleteEmployee(employeeId);
	}

	@Override
	public boolean removeTicket(int ticketId) {

		return reburseObj.deleteTicket(ticketId);
	}

	

}

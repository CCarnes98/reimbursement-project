package main.toplayer.server;

import java.util.List;
import main.toplayer.model.*;

public interface ReimbursementService {
	//inserts
	public Boolean AddEmployee(Employee employee);
	public Boolean AddTicket(Ticket ticket);
	
	//selects
	public Employee getEmployee(UserAccount loginUser);
	public List<Employee> getAllEmployees();
	public Ticket getTicket(int ticketId);
	public List<Ticket> getAllTickets();
	
	//updates
	public boolean updateTicketStatus(int ticketId, String Status);
	
	//deletes
	public boolean removeEmployee(int employeeId);
	public boolean removeTicket(int ticketId);
	
}
